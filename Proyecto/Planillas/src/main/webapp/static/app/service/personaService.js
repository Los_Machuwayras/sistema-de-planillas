angular.module('personaService', [])

.factory('PersonaService', function($http) {

    return {
        // get all the comments
        get : function() {
            return $http.get('/api/personas/all');
        },

        // save a comment (pass in comment data)
        save : function(personaData) {
            return $http({
                method: 'POST',
                url: '/api/personas',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(personaData)
            });
        },

        // destroy a comment
        destroy : function(id) {
            return $http.delete('/api/personas/' + id);
        }
    }

});