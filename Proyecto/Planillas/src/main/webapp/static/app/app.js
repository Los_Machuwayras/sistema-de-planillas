var unajApp = angular.module('planillasApp', [
	'personaCtrl', 
	'personaService'
], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    }); 
