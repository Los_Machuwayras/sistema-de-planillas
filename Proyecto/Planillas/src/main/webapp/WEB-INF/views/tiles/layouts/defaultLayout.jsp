<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 
<html>
 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><tiles:getAsString name="title" /></title>
    <!-- Bootstrap 3.3.5 -->
    <link href="<c:url value='/static/bower_components/bootstrap/dist/css/bootstrap.min.css'/>" rel="stylesheet" >
    <!-- Font Awesome -->
    <link href="<c:url value='/static/bower_components/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" >
    <!-- Ionicons -->
    <link rel="stylesheet" href="<c:url value='/static/bower_components/ionicons/css/ionicons.min.css' />" >
    <!-- Theme style -->
    <link rel="stylesheet" href="<c:url value='/static/css/AdminLTE.min.css'/>" >
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<c:url value='/static/css/skins/_all-skins.min.css' />" >
    <!-- iCheck -->
    <link rel="stylesheet" href="<c:url value='/static/bower_components/iCheck/skins/flat/blue.css' />" >

    <!-- Scripts -->
    
    <!-- jQuery 2.1.4 -->
    <script src="<c:url value='/static/bower_components/jquery/dist/jquery.min.js' />"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<c:url value='/static/bower_components/jquery-ui/ui/jquery-ui.js' />"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<c:url value='/static/bower_components/bootstrap/dist/js/bootstrap.min.js' />"></script>
    <!-- AdminLTE App -->
    <script src="<c:url value='/static/js/app.min.js' />" ></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--script src="/static/js/pages/dashboard.js"></script-->
    <!-- AdminLTE for demo purposes -->
    <script src="<c:url value='/static/js/demo.js' />" ></script>

    <script type="text/javascript" src="<c:url value='/static/bower_components/angular/angular.min.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-resource/angular-resource.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-cookies/angular-cookies.js' />" ></script>
    
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-sanitize/angular-sanitize.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-animate/angular-animate.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-bootstrap/ui-bootstrap.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-bootstrap/ui-bootstrap-tpls.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-touch/angular-touch.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-toasty/js/ng-toasty.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/sweetalert/lib/sweet-alert.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-sweetalert/SweetAlert.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/static/bower_components/angular-file-upload/angular-file-upload.min.js' />" ></script>
      
    <!-- Scripts Angular JS -->
    <script type="text/javascript" src="<c:url value='/static/app/app.js' />" ></script>
    <!-- Controllers -->
    <script type="text/javascript" src="<c:url value='/static/app/controller/personaController.js' />" ></script>
    
    <!-- Admin Ctrls -->
    <!-- Services -->
    <script type="text/javascript" src="<c:url value='/static/app/service/personaService.js' />" ></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    
</head>
  
<body class="hold-transition skin-blue sidebar-mini" ng-app="planillasApp">
	<div id="app" class="wrapper">
        <header id="header" class="main-header">
            <tiles:insertAttribute name="header" />
        </header>
     
        <section id="sidemenu">
            <tiles:insertAttribute name="menu" />
        </section>
             
        <section id="site-content" class="content-wrapper">
            <tiles:insertAttribute name="body" />
        </section>
         
        <footer id="footer" class="main-footer">
            <tiles:insertAttribute name="footer" />
        </footer>
	</div>
</body>
</html>