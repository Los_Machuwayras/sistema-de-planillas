package com.mp.planillas.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mp.planillas.dao.Persona;

@RestController
@RequestMapping("/personas")
public class PersonasService {
	
	@RequestMapping(value = "/demo", method = RequestMethod.GET)
	public ResponseEntity<Persona> demo() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("success", "true");
		data.put("data", "ok");
		Persona persona = new Persona();
		persona.setApellidoMaterno("apellidoMaterno");
		persona.setNombres("nombres");
        return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}
}
