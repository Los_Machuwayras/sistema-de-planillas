// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Reniec;
import com.mp.planillas.dao.ReniecId;

/**
 * Home object for domain model class Reniec.
 * @see .Reniec
 * @author Hibernate Tools
 */
@Stateless
public class ReniecHome {

	private static final Log log = LogFactory.getLog(ReniecHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Reniec transientInstance) {
		log.debug("persisting Reniec instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Reniec persistentInstance) {
		log.debug("removing Reniec instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Reniec merge(Reniec detachedInstance) {
		log.debug("merging Reniec instance");
		try {
			Reniec result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Reniec findById(ReniecId id) {
		log.debug("getting Reniec instance with id: " + id);
		try {
			Reniec instance = entityManager.find(Reniec.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
