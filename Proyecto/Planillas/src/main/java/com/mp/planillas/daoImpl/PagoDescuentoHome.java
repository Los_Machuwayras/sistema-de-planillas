// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.PagoDescuento;

/**
 * Home object for domain model class PagoDescuento.
 * @see .PagoDescuento
 * @author Hibernate Tools
 */
@Stateless
public class PagoDescuentoHome {

	private static final Log log = LogFactory.getLog(PagoDescuentoHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(PagoDescuento transientInstance) {
		log.debug("persisting PagoDescuento instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(PagoDescuento persistentInstance) {
		log.debug("removing PagoDescuento instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public PagoDescuento merge(PagoDescuento detachedInstance) {
		log.debug("merging PagoDescuento instance");
		try {
			PagoDescuento result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public PagoDescuento findById(int id) {
		log.debug("getting PagoDescuento instance with id: " + id);
		try {
			PagoDescuento instance = entityManager
					.find(PagoDescuento.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
