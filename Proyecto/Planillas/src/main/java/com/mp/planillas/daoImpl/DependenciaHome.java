// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Dependencia;

/**
 * Home object for domain model class Dependencia.
 * @see .Dependencia
 * @author Hibernate Tools
 */
@Stateless
public class DependenciaHome {

	private static final Log log = LogFactory.getLog(DependenciaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Dependencia transientInstance) {
		log.debug("persisting Dependencia instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Dependencia persistentInstance) {
		log.debug("removing Dependencia instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Dependencia merge(Dependencia detachedInstance) {
		log.debug("merging Dependencia instance");
		try {
			Dependencia result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Dependencia findById(int id) {
		log.debug("getting Dependencia instance with id: " + id);
		try {
			Dependencia instance = entityManager.find(Dependencia.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
