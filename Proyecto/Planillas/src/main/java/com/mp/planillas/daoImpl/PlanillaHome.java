// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Planilla;

/**
 * Home object for domain model class Planilla.
 * @see .Planilla
 * @author Hibernate Tools
 */
@Stateless
public class PlanillaHome {

	private static final Log log = LogFactory.getLog(PlanillaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Planilla transientInstance) {
		log.debug("persisting Planilla instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Planilla persistentInstance) {
		log.debug("removing Planilla instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Planilla merge(Planilla detachedInstance) {
		log.debug("merging Planilla instance");
		try {
			Planilla result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Planilla findById(Integer id) {
		log.debug("getting Planilla instance with id: " + id);
		try {
			Planilla instance = entityManager.find(Planilla.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
