package com.mp.planillas.dao;

// Generated 08/01/2017 09:55:34 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Reniec generated by hbm2java
 */
@Entity
@Table(name = "Reniec", catalog = "planillas")
public class Reniec implements java.io.Serializable {

	private ReniecId id;

	public Reniec() {
	}

	public Reniec(ReniecId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "dni", column = @Column(name = "DNI", nullable = false, length = 12)),
			@AttributeOverride(name = "carRes", column = @Column(name = "Car_res", length = 1)),
			@AttributeOverride(name = "numLib", column = @Column(name = "NumLib", length = 6)),
			@AttributeOverride(name = "ubibeo", column = @Column(name = "Ubibeo", length = 6)),
			@AttributeOverride(name = "apellidoPaterno", column = @Column(name = "ApellidoPaterno", length = 40)),
			@AttributeOverride(name = "apellidoMaterno", column = @Column(name = "ApellidoMaterno", length = 40)),
			@AttributeOverride(name = "nombres", column = @Column(name = "Nombres", nullable = false, length = 50)),
			@AttributeOverride(name = "columna8", column = @Column(name = "Columna8", length = 8)),
			@AttributeOverride(name = "genero", column = @Column(name = "Genero", length = 1)),
			@AttributeOverride(name = "gradoInstruccion", column = @Column(name = "GradoInstruccion", length = 2)),
			@AttributeOverride(name = "tipoDocumento", column = @Column(name = "TipoDocumento", length = 1)) })
	public ReniecId getId() {
		return this.id;
	}

	public void setId(ReniecId id) {
		this.id = id;
	}

}
