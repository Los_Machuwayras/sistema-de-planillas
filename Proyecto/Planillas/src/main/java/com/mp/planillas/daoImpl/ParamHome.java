// default package
package com.mp.planillas.daoImpl;

// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Param;
import com.mp.planillas.dao.ParamId;

/**
 * Home object for domain model class Param.
 * @see .Param
 * @author Hibernate Tools
 */
@Stateless
public class ParamHome {

	private static final Log log = LogFactory.getLog(ParamHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Param transientInstance) {
		log.debug("persisting Param instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Param persistentInstance) {
		log.debug("removing Param instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Param merge(Param detachedInstance) {
		log.debug("merging Param instance");
		try {
			Param result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Param findById(ParamId id) {
		log.debug("getting Param instance with id: " + id);
		try {
			Param instance = entityManager.find(Param.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
