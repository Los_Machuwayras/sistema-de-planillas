package com.mp.planillas.dao;

// Generated 08/01/2017 09:55:34 AM by Hibernate Tools 3.4.0.CR1

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Beneficiario generated by hbm2java
 */
@Entity
@Table(name = "Beneficiario", catalog = "planillas")
public class Beneficiario implements java.io.Serializable {

	private Integer id;
	private Planilla planilla;
	private Persona persona;
	private BigDecimal porcentaje;
	private BigDecimal total;

	public Beneficiario() {
	}

	public Beneficiario(Planilla planilla, Persona persona,
			BigDecimal porcentaje, BigDecimal total) {
		this.planilla = planilla;
		this.persona = persona;
		this.porcentaje = porcentaje;
		this.total = total;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPlanilla", nullable = false)
	public Planilla getPlanilla() {
		return this.planilla;
	}

	public void setPlanilla(Planilla planilla) {
		this.planilla = planilla;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPersona", nullable = false)
	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Column(name = "Porcentaje", nullable = false, precision = 16)
	public BigDecimal getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Column(name = "Total", nullable = false, precision = 16)
	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
