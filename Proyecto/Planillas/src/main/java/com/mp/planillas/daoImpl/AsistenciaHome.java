package com.mp.planillas.daoImpl;

// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Asistencia;

/**
 * Home object for domain model class Asistencia.
 * @see .Asistencia
 * @author Hibernate Tools
 */
@Stateless
public class AsistenciaHome {

	private static final Log log = LogFactory.getLog(AsistenciaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Asistencia transientInstance) {
		log.debug("persisting Asistencia instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Asistencia persistentInstance) {
		log.debug("removing Asistencia instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Asistencia merge(Asistencia detachedInstance) {
		log.debug("merging Asistencia instance");
		try {
			Asistencia result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Asistencia findById(Integer id) {
		log.debug("getting Asistencia instance with id: " + id);
		try {
			Asistencia instance = entityManager.find(Asistencia.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
