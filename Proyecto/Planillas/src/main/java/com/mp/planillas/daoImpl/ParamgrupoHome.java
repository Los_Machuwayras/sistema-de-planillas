// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Paramgrupo;

/**
 * Home object for domain model class Paramgrupo.
 * @see .Paramgrupo
 * @author Hibernate Tools
 */
@Stateless
public class ParamgrupoHome {

	private static final Log log = LogFactory.getLog(ParamgrupoHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Paramgrupo transientInstance) {
		log.debug("persisting Paramgrupo instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Paramgrupo persistentInstance) {
		log.debug("removing Paramgrupo instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Paramgrupo merge(Paramgrupo detachedInstance) {
		log.debug("merging Paramgrupo instance");
		try {
			Paramgrupo result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Paramgrupo findById(String id) {
		log.debug("getting Paramgrupo instance with id: " + id);
		try {
			Paramgrupo instance = entityManager.find(Paramgrupo.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
