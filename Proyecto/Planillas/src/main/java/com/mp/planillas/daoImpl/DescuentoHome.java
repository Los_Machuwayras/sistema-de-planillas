// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Descuento;

/**
 * Home object for domain model class Descuento.
 * @see .Descuento
 * @author Hibernate Tools
 */
@Stateless
public class DescuentoHome {

	private static final Log log = LogFactory.getLog(DescuentoHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Descuento transientInstance) {
		log.debug("persisting Descuento instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Descuento persistentInstance) {
		log.debug("removing Descuento instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Descuento merge(Descuento detachedInstance) {
		log.debug("merging Descuento instance");
		try {
			Descuento result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Descuento findById(int id) {
		log.debug("getting Descuento instance with id: " + id);
		try {
			Descuento instance = entityManager.find(Descuento.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
