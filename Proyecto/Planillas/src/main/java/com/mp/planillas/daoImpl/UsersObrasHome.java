// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.UsersObras;
import com.mp.planillas.dao.UsersObrasId;

/**
 * Home object for domain model class UsersObras.
 * @see .UsersObras
 * @author Hibernate Tools
 */
@Stateless
public class UsersObrasHome {

	private static final Log log = LogFactory.getLog(UsersObrasHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(UsersObras transientInstance) {
		log.debug("persisting UsersObras instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(UsersObras persistentInstance) {
		log.debug("removing UsersObras instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public UsersObras merge(UsersObras detachedInstance) {
		log.debug("merging UsersObras instance");
		try {
			UsersObras result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UsersObras findById(UsersObrasId id) {
		log.debug("getting UsersObras instance with id: " + id);
		try {
			UsersObras instance = entityManager.find(UsersObras.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
