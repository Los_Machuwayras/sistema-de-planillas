// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Beneficiario;

/**
 * Home object for domain model class Beneficiario.
 * @see .Beneficiario
 * @author Hibernate Tools
 */
@Stateless
public class BeneficiarioHome {

	private static final Log log = LogFactory.getLog(BeneficiarioHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Beneficiario transientInstance) {
		log.debug("persisting Beneficiario instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Beneficiario persistentInstance) {
		log.debug("removing Beneficiario instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Beneficiario merge(Beneficiario detachedInstance) {
		log.debug("merging Beneficiario instance");
		try {
			Beneficiario result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Beneficiario findById(Integer id) {
		log.debug("getting Beneficiario instance with id: " + id);
		try {
			Beneficiario instance = entityManager.find(Beneficiario.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
