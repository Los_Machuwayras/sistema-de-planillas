// default package
// Generated 08/01/2017 10:02:08 AM by Hibernate Tools 3.4.0.CR1
package com.mp.planillas.daoImpl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mp.planillas.dao.Ubigeo;

/**
 * Home object for domain model class Ubigeo.
 * @see .Ubigeo
 * @author Hibernate Tools
 */
@Stateless
public class UbigeoHome {

	private static final Log log = LogFactory.getLog(UbigeoHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Ubigeo transientInstance) {
		log.debug("persisting Ubigeo instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Ubigeo persistentInstance) {
		log.debug("removing Ubigeo instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Ubigeo merge(Ubigeo detachedInstance) {
		log.debug("merging Ubigeo instance");
		try {
			Ubigeo result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Ubigeo findById(Integer id) {
		log.debug("getting Ubigeo instance with id: " + id);
		try {
			Ubigeo instance = entityManager.find(Ubigeo.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
