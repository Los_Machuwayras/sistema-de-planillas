package com.mp.planillas.dao;

// Generated 08/01/2017 09:55:34 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Paramgrupo generated by hbm2java
 */
@Entity
@Table(name = "Paramgrupo", catalog = "planillas")
public class Paramgrupo implements java.io.Serializable {

	private String grupo;
	private String nombreTabla;
	private Set<Param> params = new HashSet<Param>(0);

	public Paramgrupo() {
	}

	public Paramgrupo(String grupo, String nombreTabla) {
		this.grupo = grupo;
		this.nombreTabla = nombreTabla;
	}

	public Paramgrupo(String grupo, String nombreTabla, Set<Param> params) {
		this.grupo = grupo;
		this.nombreTabla = nombreTabla;
		this.params = params;
	}

	@Id
	@Column(name = "Grupo", unique = true, nullable = false, length = 3)
	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	@Column(name = "NombreTabla", nullable = false, length = 256)
	public String getNombreTabla() {
		return this.nombreTabla;
	}

	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "paramgrupo")
	public Set<Param> getParams() {
		return this.params;
	}

	public void setParams(Set<Param> params) {
		this.params = params;
	}

}
